"""
old, less readable script
- iterates through all book cover urls parsed from goodreads
- reverse image searches that book cover using the url
- selects the 'large' images, and saves the links to those high resolution images
"""
import datetime
import traceback
from multiprocessing.pool import ThreadPool

from lxml import etree
import pymongo
from selenium import webdriver

from tools.Util import *

AMOUNT_OF_THREADS = 4
AMOUNT_TO_PARSE = 9 + 1

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "full_parse_results"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

base_url = "https://www.google.com/searchbyimage?image_url="
runtime_start = datetime.datetime.now()
amount_parsed = 0


def parse_hd_covers(book_dict):
    try:

        # setup headless browser
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        browser = webdriver.Chrome(options=options)

        start_time = datetime.datetime.now()
        isbn = book_dict["isbn"]
        reverse_url = f"{base_url}{book_dict['cover_link']}"
        browser.get(reverse_url)
        data = browser.page_source
        # print(f"Data: {data}")
        tree = etree.HTML(data)
        xpath = "//span[@class='gl']//a/@href"
        results = tree.xpath(xpath)
        large_img_url = None
        for x in results:
            large_img_url = "https://google.com" + x
        # print(f"Large images URL {large_img_url} determined from {url}")

        browser.get(large_img_url)
        data = browser.page_source
        browser.close()
        # print(f"Received: {data}")

        # apply le regex
        image_url_regex = r"(\[.*\.(jpg|jpeg).*\])"
        matches = re.findall(image_url_regex, data)
        # print(f"Matches {matches}")

        # for all images on page: parse properties, put in dict, put that in list
        results = []
        count = 1
        for match in matches:
            # match example: ["https://i2.books-express.ro/bt/9780099511021/educated.jpg",225,147]
            url = re.findall(r'"(.*?)"', str(match))[0]
            results.append(url)
            count += 1
            if count >= AMOUNT_TO_PARSE:
                break

        # this item time
        end_time = datetime.datetime.now()

        time_diff = (end_time - start_time)
        execution_time = int(time_diff.total_seconds() * 1000)

        # average time
        global amount_parsed
        amount_parsed += 1
        time_diff = (end_time - runtime_start)
        f_execution_time = int(time_diff.total_seconds() * 1000)
        avg_parse_time = round(f_execution_time / amount_parsed)
        update_dict = {
            "hd_cover_page": reverse_url,
            "hd_large_cover_page": large_img_url,
            "hd_parse_duration": execution_time
        }

        count = 1
        for parsed_img in results:
            key = f"hd_cover_link_{count}"
            value = parsed_img
            update_dict[key] = value
            count += 1
            if count >= AMOUNT_TO_PARSE:
                break

        db_collection.update_one({"isbn": isbn}, {"$set": update_dict})
        print(f"{isbn}: {len(update_dict)} new items in {execution_time}ms({avg_parse_time}): {update_dict}")
    except Exception as e:
        print(f"Failed to find HD covers for {book_dict}")

        update_dict = {
            "hd_parse_failed": True,
        }
        db_collection.update_one({"isbn": book_dict["isbn"]}, {"$set": update_dict})


        try:
            browser.close()
        except Exception as e:
            print(f"Tried to close browser, but {e}")

        # print(traceback.format_exc())
        print(e)


# put all data from database in list
book_cursor = db_collection.find({})
book_dict_list = []
isbn_list = []
for book_dict in book_cursor:
    book_dict_list.append(book_dict)
    isbn_list.append(book_dict["isbn"])

# filter items from list which are already parsed
print("Filtering out books of which the covers are already parsed or have failed to parse.")
book_cursor = db_collection.find({"$and": [{"hd_cover_link_1": {"$exists": True}},
                                           {"hd_cover_link_1": {"$ne": ""}},
                                           ]})

parse_failed_cursor = db_collection.find({"$and": [{"hd_parse_failed": {"$exists": True}},
                                           {"hd_parse_failed": {"$ne": ""}},
                                           ]})
original_size = len(book_dict_list)

for book_dict in parse_failed_cursor:
    isbn_already_in_db = book_dict["isbn"]
    if isbn_already_in_db in isbn_list:
        if book_dict in book_dict_list:
            book_dict_list.remove(book_dict)

amount_removed = original_size - len(book_dict_list)
print(f"Filtered {amount_removed} items which failed to parse.")

original_size = len(book_dict_list)

for book_dict in book_cursor:
    isbn_already_in_db = book_dict["isbn"]
    if isbn_already_in_db in isbn_list:
        if book_dict in book_dict_list:
            book_dict_list.remove(book_dict)
        # print(f"Removed {isbn_already_in_db}")
amount_removed = original_size - len(book_dict_list)
print(f"Filtered {amount_removed} items already parsed.")

# start parsing
print(f"Started parsing HD covers of {len(book_dict_list)} books.")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(parse_hd_covers, book_dict_list)

# finish parsing
end_time = datetime.datetime.now()
time_diff = (end_time - runtime_start)
execution_time = int(time_diff.total_seconds() * 1000)
print(f"Script runtime: {execution_time}ms.")
print(f"Parsing finished!")
