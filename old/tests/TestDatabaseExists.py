import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "full_parse_results"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

# book_cursor = db.collection.find({"isbn": { "$exists": True}})
# book_cursor = db_collection.find({})

# book_cursor = db.collection.find({"hd_cover_link_1": { "$exists": True, "$ne": None}})
book_cursor = db_collection.find({"$and": [{"hd_cover_link_1": {"$exists": True}}, {"hd_cover_link_1": {"$ne": ""}}]})

count = 0
for dic in book_cursor:
    print(f"Dict: {dic}")
    count += 1
    if count > 5:
        break
