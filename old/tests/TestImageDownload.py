from PIL import Image
import requests
from io import BytesIO


url = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1482891316l/29779231.jpg"
response = requests.get(url)
img = Image.open(BytesIO(response.content))
# img.show()
source_width, source_height = img.size
print(f"IMG SIZE: {img.size[0]}, type{type(img.size[0])}")
print(f"Width={source_width}, height={source_height}")
