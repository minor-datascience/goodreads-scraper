from serpapi.google_search_results import GoogleSearchResults

params = {
  "engine": "google_reverse_image",
  "image_url": "https://i.imgur.com/5bGzZi7.jpg",
  "api_key": "secret_api_key"
}

client = GoogleSearchResults(params)
results = client.get_dict()
inline_images = results['inline_images']