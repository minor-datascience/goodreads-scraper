# script for testing mongodb database functions
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "the_data"

# database created automatically when content is added
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient[DATABASE_NAME]

dblist = myclient.list_database_names()

if DATABASE_NAME in dblist:
    print("The database exists.")
else:
    print("Database does not exist.")

# create a collection/table, created automatically when it gets content
mycol = mydb[COLLECTION_NAME]

collist = mydb.list_collection_names()
if COLLECTION_NAME in collist:
    print("Collection exists")
else:
    print("Collection does not exist.")

mydict = {"name": "Jort", "address": "Home"}

# insert document/record/data entry in collection/table
# returns object which has a property and inserted_id
# insert_one accepts dicts, insert_many accepts list of dicts
# no _id was specified, so unique id is generated automatically for this document (put _id in dict)
x = mycol.insert_one(mydict)
print("Inserted ID:", x.inserted_id)

mydict = {"name": "Peter", "address": "Lowstreet 27"}

x = mycol.insert_one(mydict)

print("Inserted ID 2:", x.inserted_id)

# find takes the query object, nothing == *
x = mycol.find_one()
print("Find result:", x)

query = {
            "isbn": "haha"
        }, {
            "cover_url": 1
        }
for x in mycol.find(query):
    print(x)
