# setup database connection
import pymongo

DATABASE_NAME = "goodreads"
TABLE_NAME = "the_data_2"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev")

db = db_client[DATABASE_NAME]
db_collection_mine = db[TABLE_NAME]

test_isbn = "9780141033570"
query = {"isbn": test_isbn}  # select all where isbn = test_isbn

db_collection_mine.update_one({"isbn": test_isbn}, {"$set": {"NIMA_score": "5.0"}})
