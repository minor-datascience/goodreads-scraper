# setup database connection
import pymongo

DATABASE_NAME = "goodreads"
TABLE_NAME = "the_data_2"

db_client = pymongo.MongoClient("mongodb://localhost:27017/")

db = db_client[DATABASE_NAME]
db_collection_mine = db[TABLE_NAME]

test_isbn = "9780812988406"
query = {"isbn" : test_isbn} # select all where isbn = test_isbn

# parse all results
book_cursor = db_collection_mine.find(query)
for book_dict in book_cursor:
    print(book_dict)
    print(book_dict["cover_link"])

# parse one result
book_dict = db_collection_mine.find_one(query)
print(book_dict)
print(book_dict["cover_link"])

