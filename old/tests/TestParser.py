# test webscraper for goodreads
import datetime

import urllib3
from bs4 import BeautifulSoup
from lxml import etree

from tools.Util import *

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def parse_book_details(isbn):
    # setup scraping tools
    url = "https://www.goodreads.com/book/isbn_to_id?isbn=" + isbn
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    response_text = response.data.decode()
    print("URL:", url)

    soup = BeautifulSoup(response_text, 'html.parser')
    tree = etree.HTML(response_text)

    # todo: published
    try:
        regex = "Published\s*(\d\d\d\d)"
        datee = get_regex_group(response_text, regex, 1)
        print("Published:", datee)
    except:
        pass

    try:
        regex = "first published.* (\d\d\d\d)"
        datee = get_regex_group(response_text, regex, 1)
        print("First published:", datee)
    except:
        pass


book = parse_book_details("9781572245136")
book = parse_book_details("9780099590088")
print(book)

