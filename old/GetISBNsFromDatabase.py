# script that prints out database contents
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "full_parse_results"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]
amount_downloaded = 0

book_cursor = db_collection.find({})

book_dict_list = []
for book_dict in book_cursor:
    book_dict_list.append(book_dict)

print(book_dict_list[:10])

