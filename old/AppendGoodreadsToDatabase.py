import time

import pymongo
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

import xml.etree.ElementTree as ET

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "kaggle_data"

CONSUMER_KEY = "fVfK4pnsbfKiS92eIqcGg"
CONSUMER_SECRET = "h3c7ezID7pCu4n2thKTBqwnbBftmFFYJST62NqBNQY"

db_client = pymongo.MongoClient("mongodb://localhost:27017/")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

row_cursor = db_collection.find({}, {
    "isbn": 1
})

for row_dict in row_cursor:
    url = "https://www.goodreads.com/book/isbn/" + str(row_dict["isbn"]) + "?key=" + CONSUMER_KEY
    print("ISBN:", row_dict["isbn"])
    print("URL:", url)
    http = urllib3.PoolManager()
    r = http.request("GET", url)
    response = r.data.decode()
    if r.status != 200:
        print("Status not 200.")
        print("Raw received response:", response)
        break

    print("Interpreted response:")
    haha  = response.translate({ord(i): None for i in "&"})
    # print(haha)
    root = ET.fromstring(haha)
    man = root.findall("work")

    for ss in root.iter("work"):
        print(ss.attrib)
    print("Man:", man)
    print("roottag:", root.tag)
    print("attrib:", root.attrib)

    time.sleep(1)
