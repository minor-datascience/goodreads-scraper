"""
NOTE: just searching for the title seems to find better resolution images:
https://www.google.com/searchbyimage?image_url=https://i.imgur.com/HBrB8p0.png
https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348806677l/1488716.jpg
TO: https://www.google.com/searchbyimage?image_url=https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348806677l/1488716.jpg
"""
import re
from io import BytesIO

import requests
from PIL import Image
from selenium import webdriver


def get_regex_group(text, regex, group):
    return re.compile(regex).search(text).group(group)


# to parse
test_isbn = "9781784701994"
test_isbn_url = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1482891316l/29779231.jpg"
g_url = f"https://www.google.com/search?q={test_isbn}&tbm=isch"
base_url = "https://www.google.com/searchbyimage?image_url="

# prepare the option for the chrome driver
options = webdriver.ChromeOptions()
options.add_argument('headless')

# start chrome browser
browser = webdriver.Chrome(options=options)
browser.get(g_url)

# get loaded page code and stop
data = browser.page_source
browser.quit()

# apply le regex
image_url_regex = r"(\[.*\.(jpg|jpeg).*\])"
matches = re.findall(image_url_regex, data)

# get original image properties
response = requests.get(test_isbn_url)
original_img = Image.open(BytesIO(response.content))
x, y = original_img.size
original_specs = {"x": x, "y": y, "xy": x * y, "x/y": round(x / y, 2)}
print(f"ORIGINAL: {original_specs}")

# for all images on page: parse properties, put in dict, put that in list
results = []
for match in matches:
    # match example: ["https://i2.books-express.ro/bt/9780099511021/educated.jpg",225,147]
    # get resolution of image from the match
    resolution_text = match[0]
    resolution_regex = r"\",(\d*,\d*)\]"
    resolution = get_regex_group(resolution_text + "", resolution_regex, 1)
    split = resolution.split(",")
    x = int(split[0])
    y = int(split[1])

    # get url of image
    url = re.findall(r'"(.*?)"', str(match))[0]

    # put image properties in dict and put that in result list
    results.append({"x": x, "y": y, "xy": x * y, "x/y": round(x / y, 2), "url": url})

# sort images by resolution
results = sorted(results, key=lambda item: item["xy"])

delta = 0.03
for parsed_img in results:
    if parsed_img["x/y"] + delta > original_specs["x/y"] > parsed_img["x/y"] - delta:
        print(f"In bounds: {parsed_img}")

print(f"ORIGINAL: {original_specs}")
print(f"USED URL: {g_url}")
