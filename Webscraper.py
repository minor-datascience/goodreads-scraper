"""
webscraper for goodreads
requires mongodb with a database with all the isbn to iterate
use tools/KaggleCSVToDatabase.py to create and fill that db
"""
import datetime
import os
from multiprocessing.pool import ThreadPool
from pathlib import Path
import traceback

import pymongo
import urllib3
from lxml import etree
from bs4 import BeautifulSoup
from tools.Util import *

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

DATABASE_NAME = "goodreads"
COLLECTION_KAGGLE_NAME = "kaggle_data"  # table/collection name with isbns to parse
COLLECTION_MINE_NAME = "full_parse_results"  # table/collection name where parsed data is written
ERROR_VALUE = "-1"  # value written when data field could not be parsed
DOWNLOADED_PAGE_FOLDER = "C:/THIS/scraping/downloadedpages/"  # folder where downloaded webpages are found
# DOWNLOADED_PAGE_FOLDER = "C:/this/parsedpages/"  # folder where downloaded webpages are found
AMOUNT_OF_THREADS = 3  # more than 3 cores do not seem to improve average parse time
TESTING = False

book_list = []  # list of parsed books dicts
count = 0  # count for stopping condition
isbn_list_to_parse = []  # list of all the isbns to parse
runtime_start = datetime.datetime.now()

# database connection
db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")
db = db_client[DATABASE_NAME]
db_collection_kaggle = db[COLLECTION_KAGGLE_NAME]
db_collection_mine = db[COLLECTION_MINE_NAME]

# stop if web page folder does not exist
if not os.path.exists(DOWNLOADED_PAGE_FOLDER):
    print(f"Web page folder '{DOWNLOADED_PAGE_FOLDER}' does not exist.")
    exit()


# this function not used anymore
def is_isbn_in_db(isbn_to_check):
    return db_collection_mine.count_documents({"isbn": isbn_to_check}) > 0


def parse_book_details(isbn):
    start_time = datetime.datetime.now()
    # read file and setup scraping tools##############################################################################
    # no try/except because uhhh
    filename = DOWNLOADED_PAGE_FOLDER + isbn + ".html"

    # skip file if not found in folder
    if not os.path.isfile(filename):
        print(f"Failed to scrape {isbn}: {filename} does not exist.")
        return

    response_text = Path(filename).read_text(encoding="utf-8")

    soup = BeautifulSoup(response_text, 'html.parser')
    tree = etree.HTML(response_text)

    # initialize result########################################################################################
    book = {}
    book["isbn"] = isbn
    # url that should have been scraped if not reading from file
    url = "https://www.goodreads.com/book/isbn_to_id?isbn=" + isbn
    book["scraped_link"] = url

    # title########################################################################################
    try:
        tag = soup.find("h1")
        book_title = get_string(tag.contents[0])
        book["title"] = book_title
    except:
        book["title"] = ERROR_VALUE

    # cover link########################################################################################
    try:
        tag = soup.select("img#coverImage")[0]  # use css selector to get by id
        book_cover_image = tag["src"]
        book_cover_image = remove_image_scaling(book_cover_image)
        book["cover_link"] = book_cover_image
    except:
        book["cover_link"] = ERROR_VALUE

    # average rating########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "ratingValue"})
        book_rating = get_string(tag.contents[0])
        book["average_rating"] = book_rating
    except:
        book["average_rating"] = ERROR_VALUE

    # amount of ratings per star########################################################################################
    try:
        xpath = "//script[contains(@type,'protovis')]"
        tag = tree.xpath(xpath)[0]
        tag_text = get_string(tag.text)
        pattern = re.compile("renderRatingGraph\(\[(.*)\]\)")
        match = pattern.match(tag_text)
        result = match.group(1)  # string captured in group 1: something like ._SY475_
        result = result.split(", ")
        book["5_star_count"] = result[0].strip()
        book["4_star_count"] = result[1].strip()
        book["3_star_count"] = result[2].strip()
        book["2_star_count"] = result[3].strip()
        book["1_star_count"] = result[4].strip()
    except:
        for x in range(0, 5):
            book[f"{x + 1}_star_count"] = ERROR_VALUE

    # writer########################################################################################
    try:
        xpath = "//meta[contains(@property,'books:author')]"
        tag = tree.xpath(xpath)[0]
        tag_text = tag.get("content")
        book["writer"] = get_regex_group(tag_text, "\/author\/show\/(.*)\.(.*)", 2).replace("_", " ")
        book["writer_id"] = get_regex_group(tag_text, "\/author\/show\/(.*)\.(.*)", 1)
    except:
        book["writer"] = ERROR_VALUE
        book["writer_id"] = ERROR_VALUE

    # amount of written reviews########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "reviewCount"})
        book_written_review_count = tag.get("content")
        book["review_count"] = book_written_review_count
    except:
        book["review_count"] = ERROR_VALUE

    # book format: paperback, etc#####################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "bookFormat"})
        book_format = get_string(tag.contents[0])
        book["format"] = book_format
    except:
        book["format"] = ERROR_VALUE

    # amount of pages########################################################################################
    try:
        tag = soup.find(attrs={"itemprop": "numberOfPages"})
        book_format = before(get_string(tag.contents[0]), " pages")
        book["amount_of_pages"] = book_format
    except:
        book["amount_of_pages"] = ERROR_VALUE

    # published date########################################################################################
    try:
        book["published"] = get_regex_group(response_text, "Published(?s).*?(\d\d\d\d)", 1)
    except:
        book["published"] = ERROR_VALUE

    # first published date  ########################################################################################
    try:
        book["first_published"] = get_regex_group(response_text, "first published(?s).*?(\d\d\d\d)", 1)
    except:
        book["first_published"] = ERROR_VALUE

    # genres########################################################################################
    try:
        book_genre_list_text = get_regex_group(response_text, 'googletag\.pubads\(\)\.setTargeting\("shelf", \[(.*)\]',
                                               1)
        book_genre_list_text = book_genre_list_text.replace('"', "")
        book_genre_list = book_genre_list_text.split(",")
        for x in range(0, len(book_genre_list)):
            key = "genre_" + str(x)
            book[key] = book_genre_list[x].strip()
    except:
        # write only 1 genre indicating there were none found
        key = "genre_0"
        book[key] = ERROR_VALUE

    # finishing up########################################################################################
    end_time = datetime.datetime.now()

    time_diff = (end_time - start_time)
    execution_time = int(time_diff.total_seconds() * 1000)
    book["parse_duration"] = execution_time

    return book


# this function could be merged with the parse_book_details function
def parse_book_details_parent(isbn):
    try:
        # parse book details as dict
        parsed_book = parse_book_details(isbn)

        # if no result (file not found)
        if not parsed_book:
            return

        # add dict to database
        db_collection_mine.insert_one(document=parsed_book)

        # add dict with parsed values to result list
        book_list.append(parsed_book)

        # gather parse statistics
        amount_of_parsed_books = len(book_list)
        end_time = datetime.datetime.now()
        time_diff = (end_time - runtime_start)
        execution_time = int(time_diff.total_seconds() * 1000)
        avg_parse_time = round(execution_time / amount_of_parsed_books)

        # print parse results
        print(f"Parsed time: {parsed_book['parse_duration']}/{avg_parse_time}: {parsed_book}")
        if TESTING:
            print_dict(parsed_book)
            print("END -  ###################################################################################")

    except Exception as e:
        print("Parsing FAILED for book with ISBN:", isbn, "with error:", e)
        if TESTING:
            print(traceback.format_exc())


########################################################################################################################
if TESTING:
    to_test = [
        "9780882848198",
        "9781780191416",
        "9780008197032",
        "9780793533923",
        "9780008327323"
    ]
    for x in to_test:
        parse_book_details_parent(x)
    print("Testing done.")
    exit()
########################################################################################################################

# collect isbns to parse from database
print("Reading ISBNs to parse.")
isbn_cursor = db_collection_kaggle.find({}, {
    "isbn": 1
})
for isbn_dict in isbn_cursor:
    isbn_list_to_parse.append(str(isbn_dict["isbn"]))
print(f"{len(isbn_list_to_parse)} ISBNs need to be parsed in total.")

# filter isbns already in database
print("Filtering out ISBNs which are already in result database.")
isbn_cursor = db_collection_mine.find({}, {
    "isbn": 1
})
original_size = len(isbn_list_to_parse)
for isbn_dict in isbn_cursor:
    isbn_already_in_db = isbn_dict["isbn"]
    if isbn_already_in_db in isbn_list_to_parse:
        isbn_list_to_parse.remove(isbn_already_in_db)
        # print(f"Removed {isbn_already_in_db}")
amount_removed = original_size - len(isbn_list_to_parse)
print(f"Filtered {amount_removed} ISBNs already in database.")

# start parsing
print(f"Started parsing {len(isbn_list_to_parse)} ISBNs.")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(parse_book_details_parent, isbn_list_to_parse)

# finish parsing
end_time = datetime.datetime.now()
time_diff = (end_time - runtime_start)
execution_time = int(time_diff.total_seconds() * 1000)
print(f"Script runtime: {execution_time}ms.")
print(f"Parsing finished!")
