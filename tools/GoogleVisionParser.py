"""
Script that retrieved book cover image information from Google Vision, and puts the needed information in the database.
Imports the Google Cloud client library, you need to download that somewhere.
"""
import json
import os
from multiprocessing.pool import ThreadPool

from google.cloud.vision_v1.proto.image_annotator_pb2 import AnnotateImageResponse
from google.protobuf.json_format import Parse

from tools.DatabaseTools import *
from tools.Timer import *

AMOUNT_OF_THREADS = 1  # higher than 1 and my database spaghetti puts values in wrong collections
FOLDER = "C:/this/googlevisionresponses/"

likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                   'LIKELY', 'VERY_LIKELY')

amount_scraped = 0
start_timer("runtime")


# saves object data of each object found in the response in the database and returns the amount saved
def scrape_object_data(isbn, response):
    set_table("objects")
    objs = response.localized_object_annotations
    amount_of_objects = 0
    for obj in objs:
        result = {"isbn": isbn, "name": obj.name, "score": obj.score}
        # print(f"Inserting {result}")
        insert_document(result)
        amount_of_objects += 1

    return amount_of_objects


# read the color analysis from the response and put that in colors database
def put_colors_in_db(isbn, response):
    set_table("colors")
    color_objects = response.image_properties_annotation.dominant_colors.colors
    for color_obj in color_objects:
        color = color_obj.color
        result = {"isbn": isbn, "red": color.red, "green": color.green, "blue": color.blue, "score": color_obj.score,
                  "pixel_fraction": color_obj.pixel_fraction}
        insert_document(result)


# saves face data of each face in the response in the faces database and returns the amount of faces found
def scrape_face_data(isbn, response):
    set_table("faces")
    faces = response.face_annotations
    amount_of_faces = 0
    for face in faces:
        joy = likelihood_name[face.anger_likelihood]
        sorrow = likelihood_name[face.sorrow_likelihood]
        anger = likelihood_name[face.anger_likelihood]
        surprise = likelihood_name[face.surprise_likelihood]
        under_exposed = likelihood_name[face.under_exposed_likelihood]
        blurred = likelihood_name[face.blurred_likelihood]
        headwear = likelihood_name[face.headwear_likelihood]
        confidence = str(format(face.detection_confidence, '.3f'))
        result = {"isbn": isbn, "joy:": joy, "sorrow": sorrow, "anger": anger, "surprise": surprise,
                  "under_exposed": under_exposed, "blurred": blurred, "headwear": headwear,
                  "face_confidence": confidence}

        insert_document(result)
        # print(f"Found and inserted face: {result}")
        amount_of_faces += 1
    return amount_of_faces


def get_google_vision_data(db_dict):
    isbn = db_dict["isbn"]
    filename = FOLDER + isbn + ".json"
    start_timer(isbn)

    # skip responses that not exists
    if not os.path.isfile(filename):
        return

    # skip files too small = google api error
    if os.path.getsize(filename) < 5000:
        return

    json_file_in = open(filename)
    data = json.load(json_file_in)
    response = Parse(data, AnnotateImageResponse())  # pass blank protocol buffer message to parse into

    # parse response and add data to linked tabled
    amount_of_objects = scrape_object_data(isbn, response)
    amount_of_faces = scrape_face_data(isbn, response)
    put_colors_in_db(isbn, response)
    image_text = response.full_text_annotation.text

    # update main table
    set_table("full_parse_results")
    main_db_insert = {"amount_of_faces": amount_of_faces, "vision_text": image_text,
                      "amount_of_objects": amount_of_objects}
    update_document("isbn", isbn, main_db_insert)

    # print statistics
    global amount_scraped
    amount_scraped += 1
    print(
        f"Inserted in {get_elapsed_time(isbn)}({round(get_elapsed_time('runtime') / amount_scraped)})ms: {main_db_insert}.")


# only parse fields where there is a cover link to analyze and are not already parsed
to_scrape = cursor_to_list(get_exist_not_exists_cursor("hd_cover_link_1", "amount_of_faces"))

print(f"Started scraping {len(to_scrape)} JSON responses from Google Vision")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(get_google_vision_data, to_scrape)
print(f"{amount_scraped} of {len(to_scrape)} items scraped in {get_elapsed_time('runtime')}ms.")
