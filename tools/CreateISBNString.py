# creates a long comma separated string with all the ISBNs in the database
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "kaggle_data"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

# select all isbns
isbn_cursor = db_collection.find({}, {
    "isbn": 1
})

result = ""
for isbn_dict in isbn_cursor:
    isbn = str(isbn_dict["isbn"])
    result += isbn + ","

print(result)



