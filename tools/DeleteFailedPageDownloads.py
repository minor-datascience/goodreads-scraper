"""
script to delete failed downloads by WebpageDownloader.py, which are created by
downloading too much pages at the same time. the pages only contain an 'unexpected error' msg
and are small in size
"""
#
import os

# SAVE_FOLDER = "C:/THIS/scraping/downloadedpages/"
SAVE_FOLDER = "C:/this/parsedpages/"

if not os.path.exists(SAVE_FOLDER):
    print(f"Save folder '{SAVE_FOLDER}' does not exist.")
    exit()

amount = 0
for file_name in os.listdir(SAVE_FOLDER):
    file_path = SAVE_FOLDER + file_name

    # delete by amount of bytes of the file
    size = os.stat(file_path).st_size
    if size < 121000:
        os.remove(file_path)
        print(f"Deleted {file_name} with size {size}")
        amount += 1

print(f"Deleted {amount} files.")
