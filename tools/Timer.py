import datetime

timers = {}


def start_timer(name):
    timers[name] = datetime.datetime.now()


# returns elapsed time in milliseconds
def get_elapsed_time(name):
    if name not in timers.keys():
        print(f"{name} is not an existing timer.")
        return 1
    time_diff = datetime.datetime.now() - timers[name]
    return int(time_diff.total_seconds() * 1000)
