# script to download the webpage of a book on goodreads by ISBN. ISBNS are read from the Kaggle database
# the source code of the page is downloaded, and not dynamically loaded javascript: so only the Ctrl + u page
import datetime
import os
import pymongo
from multiprocessing.dummy import Pool as ThreadPool
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

DATABASE_NAME = "goodreads"
COLLECTION_KAGGLE_NAME = "kaggle_data"
AMOUNT_OF_THREADS = 10  # too high and goodreads blocks your ip temporarily
SAVE_FOLDER = "C:/this/parsedpages/"

# check if save folder exists
if not os.path.exists(SAVE_FOLDER):
    print(f"Save folder '{SAVE_FOLDER}' does not exist.")
    exit()

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")

db = db_client[DATABASE_NAME]
db_collection_kaggle = db[COLLECTION_KAGGLE_NAME]


def download_isbn(isbn):
    start_time = datetime.datetime.now()

    filename = SAVE_FOLDER + isbn + ".html"

    # skip if already downloaded
    if os.path.isfile(filename):
        return

    url = "https://www.goodreads.com/book/isbn_to_id?isbn=" + isbn
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    response_text = response.data.decode("utf-8")

    text_file = open(filename, "w", encoding="utf-8")
    text_file.write(response_text)
    text_file.close()

    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    execution_time = int(time_diff.total_seconds() * 1000)
    print(f"Downloaded {filename} in {execution_time}ms")


# select all isbns
isbn_cursor = db_collection_kaggle.find({}, {
    "isbn": 1
})

isbn_list = []
for isbn_dict in isbn_cursor:
    isbn_list.append(str(isbn_dict["isbn"]))

pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(download_isbn, isbn_list)
