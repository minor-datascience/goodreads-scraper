"""
Script that downloads the responses from Google Vision
- passes the first hd_cover_link in database as parameter for API request
- encodes message to json and saves it as isbn.json

Imports the Google Cloud client library, you need to download that somewhere.

Response object Python documentation:
https://googleapis.dev/python/vision/latest/vision_v1/types.html#google.cloud.vision_v1.types.AnnotateImageResponse
Response Message object to JSON and vice versa documentation:
https://googleapis.dev/python/protobuf/latest/google/protobuf/json_format.html
"""
import json
import os
from multiprocessing.pool import ThreadPool

from google.cloud import vision
from google.cloud.vision_v1.proto.image_annotator_pb2 import AnnotateImageResponse
from google.protobuf.json_format import MessageToJson, Parse

from tools.DatabaseTools import *
from tools.Timer import *

FOLDER = "C:/this/googlevisionresponses/"
AMOUNT_OF_THREADS = 200

start_timer("runtime")
amount_downloaded = 0

# put all the existing features to request in a list
# yeah boy i understand list comprehensions
features = [{"type": feature_type} for feature_type in vision.enums.Feature.Type]
client = vision.ImageAnnotatorClient()


# request API response with URL and save it to isbn.json
def save_response_to_json(db_dict):
    isbn = db_dict["isbn"]
    url = db_dict["hd_cover_link_1"]
    start_timer(isbn)

    # skip if file exists
    filename = FOLDER + isbn + ".json"
    if os.path.isfile(filename):
        return

    # get response from API
    response = client.annotate_image({'image': {'source': {'image_uri': url}}, 'features': features})

    # convert response to JSON and save
    response_json = MessageToJson(response)
    with open(filename, 'w') as json_file_out:
        json.dump(response_json, json_file_out)

    # print statistics
    global amount_downloaded
    amount_downloaded += 1
    print(
        f"Parsed {isbn} with {url} in {get_elapsed_time(isbn)}({round(get_elapsed_time('runtime') / amount_downloaded)})ms")


# load json file from disk and AnnotateImageResponse
def load_response_from_json(isbn):
    filename = FOLDER + isbn + ".json"
    with open(filename) as json_file_in:
        data = json.load(json_file_in)
        return Parse(data, AnnotateImageResponse())  # pass blank protocol buffer message to parse into


# only parse database items which have a hd cover link
to_parse = cursor_to_list(get_exists_cursor("hd_cover_link_1"))

print(f"Started requesting {len(to_parse)} book covers analyses from Google Vision")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(save_response_to_json, to_parse)
print(f"{amount_downloaded} of {len(to_parse)} items downloaded in {get_elapsed_time('runtime')}ms.")
