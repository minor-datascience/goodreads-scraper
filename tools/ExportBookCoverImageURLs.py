# script that prints out all the book cover image urls from the database
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_MINE_NAME = "the_data"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")

db = db_client[DATABASE_NAME]
db_collection_mine = db[COLLECTION_MINE_NAME]

cursor = db_collection_mine.find({}, {"cover_link": 1})

for db_dict_item in cursor:
    print(db_dict_item["cover_link"])

