"""
script to interact with database with useful methods

USE MONGO IN SHELL (ON SERVER):
mongo --username jort --password finalround

FORCE UNIQUE FIELDS IN DB
use goodreads
db.full_parse_results.createIndex({"isbn": 1}, {unique:true})

DELETE DOCUMENTS WITH DUPLICATE FIELDS
this removes all the fields with a duplicate isbn in the full_parse_results collection, and leaves one
db.full_parse_results.aggregate(
    [
        { "$sort": { "_id": 1 } },
        { "$group": {
            "_id": "$isbn",
            "doc": { "$first": "$$ROOT" }
        }},
        { "$replaceRoot": { "newRoot": "$doc" } },
        { "$out": "full_parse_results" }
    ]
)

"""
import pymongo

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "full_parse_results"
db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")
db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]


def print_object(obj):
    for attr in dir(obj):
        print("obj.%s = %r" % (attr, getattr(obj, attr)))


# set the table the functions get applied to
def set_table(collection_name):
    global db_collection
    db_collection = db[collection_name]


# converts a cursor to a list filled with the dicts from the cursor
def cursor_to_list(cursor):
    return list(cursor)


# converts a cursor to a list of strings with only the specified field items
def cursor_to_string_list(cursor, field):
    result = []
    for x in cursor:
        result.append(x[field])
    return result


# returns cursor with all documents in collection
def get_all_cursor():
    return db_collection.find({})


def get_not_exists_cursor(*fields):
    query = []
    for field in fields:
        query.append({field: {"$exists": False}})

    return db_collection.find({"$and": query})


# returns cursor with documents which have the field
def get_exists_cursor(*fields):
    query = []
    for field in fields:
        query.append({field: {"$exists": True}})

    return db_collection.find({"$and": query})


# returns cursor with documents which have the first field, but not the second
def get_exist_not_exists_cursor(exist_field, not_exist_field):  # spaghetti function naming
    return db_collection.find({"$and": [{exist_field: {"$exists": True}}, {not_exist_field: {"$exists": False}}]})


# update the database where the field key equals value with the new values
# example: update_document("isbn", 4323453453, {"title": "Sapiens", "author_age" : 32})
# does not create a new field, set upsert=True for that
# returns true if one document was updated
def update_document(key, value, new_values_dict):
    print(f"Updating where {key} equals {value} with {new_values_dict}")
    print(f"Updating {get_single_document({'isbn': value})}")
    result = db_collection.update_one({key: value}, {"$set": new_values_dict})
    if result.modified_count == 1:
        return True
    print("Documents not updated. Aren't the values already in the database?")
    return False


# return the amount of documents where the field exists
def count_documents_containing_field(field):
    return len(cursor_to_list(get_exists_cursor(field)))


# insert one document in the database
def insert_document(document_dict):
    db_collection.insert_one(document=document_dict)


# return one document in the table matching the filter
def get_single_document(filter_dict):
    result = None
    count = 0
    for document in db_collection.find(filter_dict):
        # return the first one
        if count == 0:
            result = document

        count += 1

    # print warning if somethings off
    if count != 1:
        print(f"Found {count} documents with {filter_dict} whilst 1 requested.")

    return result


def get_collection():
    return db_collection


# removes all the values in the field in the database
# watch out: example: if you would pass 'isbn' as parameter, all the isbn values in the database get nuked
def nuke_all_values(field):
    amount_nuked = len(cursor_to_list(get_exists_cursor(field)))
    db_collection.update({}, {"$unset": {field: 1}}, multi=True)
    print(f"Nuked {amount_nuked} occurrences of {field}.")


# nukes all the documents containing a field
def nuke_all_document_containing_field(field):
    amount_nuked = len(cursor_to_list(get_exists_cursor(field)))
    db_collection.remove({
        field: {"$exists": True}
    })
    print(f"Nuked {amount_nuked} documents containing {field}.")
