"""
- iterates through all book cover urls parsed from goodreads
- reverse image searches that book cover using the url
- selects the 'large' images, and saves the links to those high resolution images
"""
import traceback
from multiprocessing.pool import ThreadPool

from lxml import etree
from selenium import webdriver

from tools.DatabaseTools import *
from tools.Timer import *
from tools.Util import *

AMOUNT_OF_THREADS = 3  # too high and it will go too fast, resulting in google block, 3 seems stable, 4 maybe
AMOUNT_TO_PARSE = 9  # max amount of hd cover link urls to parse

# variables to keep track of average parse times
start_timer("runtime")
amount_parsed = 0


# write in the database that this isbn failed to parse
def set_failed(isbn, e):
    print(f"Failed to parse {isbn}: {e}")
    update_document("isbn", isbn, {"hd_parse_failed": True})


# on a reverse image search page source code:
# search for the links to the 'small', 'medium', 'large' image version pages and return the url of the largest one
def get_largest_img_url(data):
    tree = etree.HTML(data)
    xpath = "//span[@class='gl']//a/@href"
    results = tree.xpath(xpath)
    largest_img_url = None
    for x in results:
        largest_img_url = "https://google.com" + x
    return largest_img_url


def get_browser_instance():
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    return webdriver.Chrome(options=options)


# returns a list of document dicts which are not yet parsed
def determine_books_to_parse():
    print("Loading from database.")
    # documents in the database which do not have those fields, are not yet parsed
    result = cursor_to_list(get_not_exists_cursor("hd_cover_link_1", "hd_parse_failed"))
    print(f"Found {len(result)} items to parse.")
    return result


def parse_hd_covers(book_dict):
    try:
        isbn = book_dict["isbn"]
        start_timer(isbn)

        reverse_url = "https://www.google.com/searchbyimage?image_url=" + book_dict["cover_link"]

        browser = get_browser_instance()
        # retrieve the reverse image search page
        browser.get(reverse_url)
        data = browser.page_source

        # retrieve the page with large versions of the uploaded image
        large_img_url = get_largest_img_url(data)
        if large_img_url is None:
            return set_failed(isbn, f"No large img url found at {reverse_url}")
        browser.get(large_img_url)
        data = browser.page_source

        # data which is going to be added to the document
        update_dict = {
            "hd_cover_page": reverse_url,
            "hd_large_cover_page": large_img_url,
            "hd_parse_duration": get_elapsed_time(isbn)
        }

        # find all the image items on the page, and extract the urls
        count = 0
        for match in re.findall(r"(\[.*\.(jpg|jpeg).*\])", data):
            # match example: ["https://i2.books-express.ro/bt/9780099511021/educated.jpg",225,147]
            count += 1
            try:
                url = re.findall(r'"(.*?)"', str(match))[0]
                if "\\" in url:
                    print(f"WEIRD URL: {url} in {match}")
            except Exception as e:
                print(f"Failed to extract URL from image item at {large_img_url}")
                return set_failed(isbn, f"Failed to extract URL from image item at {large_img_url} because {e}")

            update_dict[f"hd_cover_link_{count}"] = url
            if count >= AMOUNT_TO_PARSE:
                break

        # fail if no images found
        if count == 0:
            return set_failed(isbn, f"No image items found at {large_img_url}")

        # parse time statistics
        global amount_parsed
        amount_parsed += 1
        avg_parse_time = round(get_elapsed_time("runtime") / amount_parsed)

        # update database with parsed values
        print(f"{isbn}: {len(update_dict)} new items in {get_elapsed_time(isbn)}ms({avg_parse_time}): {update_dict}")
        if not update_document("isbn", isbn, update_dict):
            return set_failed(isbn, "FAILED TO UPDATE DATABASE")

    except Exception as e:
        # if something unexpected happens whilst parsing
        return set_failed(isbn, f"Unknown error: {e}")
        # print(traceback.format_exc())
    finally:
        # always close the browser, this triggers even when using return in the try block
        try:
            browser.close()
        except Exception as e:
            # error might have been occured before browser was opened
            print(f"Tried to close browser, but {e}")


book_dict_list = determine_books_to_parse()

# start parsing
print(f"Started parsing HD covers of {len(book_dict_list)} books.")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(parse_hd_covers, book_dict_list)

# finish parsing
print(f"Parsing finished! Script runtime: {get_elapsed_time('runtime')}ms.")
