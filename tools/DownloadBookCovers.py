# script downloads all the book cover image urls from the database
import os
from multiprocessing.pool import ThreadPool

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

from tools.DatabaseTools import *
from tools.Timer import *

FOLDER = "C:/this/downloadedbookcovers/"
AMOUNT_OF_THREADS = 100

start_timer("runtime")
amount_downloaded = 0


def download_cover(db_dict_item):
    try:
        isbn = db_dict_item["isbn"]
        start_timer(isbn)
        filename = FOLDER + isbn + ".jpg"

        # skip if file exists
        if os.path.isfile(filename):
            return

        url = db_dict_item["hd_cover_link_1"]
        r = requests.get(url, allow_redirects=True, verify=False, timeout=5)
        open(filename, 'wb').write(r.content)

        # parse statistics
        global amount_downloaded
        amount_downloaded += 1
        avg_parse_time = round(get_elapsed_time("runtime") / amount_downloaded)
        parse_time = get_elapsed_time(isbn)
        print(f"Downloaded {amount_downloaded}/{amount_to_download}: {url} in {parse_time}({avg_parse_time})ms")
    except Exception as e:
        print(f"Failed to download {url} from {db_dict_item}")
        print(f"Reason ({type(e)}): {e}")
        # print(traceback.format_exc())


db_dict_item_list = cursor_to_list(get_exists_cursor("hd_cover_link_1"))
amount_to_download = len(db_dict_item_list)

# path joining version for other paths
amount_already_downloaded = len([name for name in os.listdir(FOLDER) if os.path.isfile(os.path.join(FOLDER, name))])
amount_to_download = amount_to_download - amount_already_downloaded


print(f"Started downloading {amount_to_download} book covers.")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(download_cover, db_dict_item_list)
print(f"{amount_downloaded} of {amount_to_download} items downloaded in {get_elapsed_time('runtime')}ms.")
