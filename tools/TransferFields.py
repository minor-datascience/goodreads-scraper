# script to transfer fields in a document to a documents in another table
import datetime
from multiprocessing.pool import ThreadPool

import pymongo

# adjustable constants
DATABASE_CONNECTION_STRING = "mongodb://jort:finalround@jort.dev"
DATABASE_NAME = "goodreads"
SOURCE_COLLECTION_NAME = "the_data_test"
RESULT_COLLECTION_NAME = "full_parse_results"
AMOUNT_OF_THREADS = 4

# database connection
db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")
db = db_client[DATABASE_NAME]
source_collection = db[SOURCE_COLLECTION_NAME]
result_collection = db[RESULT_COLLECTION_NAME]

# other vars
runtime_start = datetime.datetime.now()
amount_transferred = 0

# load source collection in memory
source_dict_list = []
cursor = source_collection.find({})  # select all
for source_dict in cursor:
    source_dict_list.append(source_dict)


def update(source_dict):
    document_id = source_dict.get("isbn")
    field_1 = source_dict.get("NIMA_score")
    field_2 = source_dict.get("NIMA_std")
    if document_id is None or field_1 is None or field_2 is None:
        print(f"Missing field to transfer in {source_dict}")
        return

    start_time = datetime.datetime.now()
    result_collection.update_one({"isbn": document_id},
                                 {"$set": {"NIMA_score": field_1, "NIMA_std": field_2}})

    end_time = datetime.datetime.now()
    time_diff = (end_time - start_time)
    transfer_time = int(time_diff.total_seconds() * 1000)

    # parse times
    global amount_transferred
    amount_transferred += 1
    end_time = datetime.datetime.now()
    time_diff = (end_time - runtime_start)
    execution_time = int(time_diff.total_seconds() * 1000)
    avg_parse_time = round(execution_time / amount_transferred)
    print(f"Transferred {document_id} in {transfer_time}ms (avg: {avg_parse_time}ms)")


pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(update, source_dict_list)
