"""
Script to generate a CSV with isbn, average rating.
Only adds isbns which can be found in the downloaded bookcovers folder.
Needed for NIMA.
"""
import os

from tools.DatabaseTools import get_all_cursor

SOURCE_FOLDER = "C:/THIS/minor-data-science-shared/bookcovers/"
OUTPUT_FILE = "C:/THIS/minor-data-science-shared/existing_isbn_rating.csv"

print("Retrieving existing isbns from folder.")
existing_isbn_list = []
for dir_entry in os.scandir(SOURCE_FOLDER):
    isbn = dir_entry.name[:-4]
    existing_isbn_list.append(isbn)

print("Loading database into memory.")
db_dict_list = []
for db_dict in get_all_cursor():
    db_dict_list.append(db_dict)

print("Cross matching and writing to file.")
count = 0
with open(OUTPUT_FILE, mode="w") as file:
    for isbn in existing_isbn_list:
        count += 1
        if count % 1000 == 0:
            print(f"Iterated {count}")

        for db_dict in db_dict_list:
            if db_dict["isbn"] == isbn:
                row = f"{isbn}, {db_dict['average_rating']}\n"
                file.write(row)
