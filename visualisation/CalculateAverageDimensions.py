"""
Script that calculates the average dimensions of all the images in a folder.
Used to determine to which aspect ratio to scale for ScaleImages.py.
"""
import os

from PIL import Image


def calculate_average_image_dimension(directory):
    print(f"Calculating average dimensions for {directory}")
    total_width = 0
    total_height = 0
    amount_iterated = 0

    for dir_entry in os.scandir(directory):
        source_img = Image.open(dir_entry.path)
        source_width, source_height = source_img.size
        total_width += source_width
        total_height += source_height
        amount_iterated += 1

    average_width = total_width // amount_iterated
    average_height = total_height // amount_iterated
    print(f"Average width={average_width}, average height={average_height} with total {amount_iterated} iterated.")
    return average_width, average_height


SOURCE_FOLDER = "C:/THIS/minor-data-science-shared/bookcovers/"
calculate_average_image_dimension(SOURCE_FOLDER)
