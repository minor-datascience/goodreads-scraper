"""
Calculate the average R, G and B values for each image in a folder, and writes those values to
the corresponding ISBN in the database.
"""
import os
from multiprocessing.pool import ThreadPool

import cv2
import numpy

from DatabaseTools import update_document
from Timer import *

SOURCE_FOLDER = "C:/this/minor-datascience-shared/bookcovers/"
AMOUNT_OF_THREADS = 4

start_timer("runtime")
count = 0

# stop program if source folder do not exist
if not os.path.isdir(SOURCE_FOLDER):
    print(f"Source folder {SOURCE_FOLDER} does not exist.")
    quit()


def get_avg_rgb(dir_entry):
    try:
        isbn = dir_entry.name[:-4]  # remove .jpg from filename
        start_timer(isbn)

        # calculate average r, g and b values of image
        img = cv2.imread(dir_entry.path)
        avg_color_per_row = numpy.average(img, axis=0)
        b, g, r = numpy.average(avg_color_per_row, axis=0)

        # append values to corresponding isbn in database
        result_dict = {"avg_red": round(r), "avg_green": round(g), "avg_blue": round(b)}
        update_document("isbn", isbn, result_dict)

        # parse statistics
        global count
        count += 1
        if count % 100 == 0:
            avg_parse_time = round(get_elapsed_time("runtime") / count)
            print(f"{count}/{amount_to_calculate}: updated {isbn} with {result_dict} in {get_elapsed_time(isbn)}({avg_parse_time})ms.")
    except Exception as e:
        print(f"Failed to calculate {dir_entry.name}: {e}")



print("Scanning for files to calculate.")
dir_entries = []
for dir_entry in os.scandir(SOURCE_FOLDER):
    dir_entries.append(dir_entry)

amount_to_calculate = len(dir_entries)
print(f"Started calculating {amount_to_calculate} items.")

pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(func=get_avg_rgb, iterable=dir_entries)

print(f"Done in {get_elapsed_time('runtime')}ms.")
