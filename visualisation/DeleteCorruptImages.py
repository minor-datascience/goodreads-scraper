"""
Delete corrupt image files in a folder.
"""
import os

from PIL import Image

SOURCE_FOLDER = "C:/THIS/minor-data-science-shared/bookcovers_stretched/"
# SOURCE_FOLDER = "C:/THIS/minor-data-science-shared/bookcovers_stretched_90x125/"


error_count = 0
for dir_entry in os.scandir(SOURCE_FOLDER):
    try:
        img = Image.open(dir_entry.path)  # raises exception if image couldn't be opened
        img.verify()  # raises exception if there is a problem with the image
    except Exception as e:
        error_count += 1
        os.remove(dir_entry.path)
        print(f"Delete #{error_count} {dir_entry.name} because {e}")

print(f"Removed {error_count} files.")

