# %%


from tools.DatabaseTools import *
import pandas as pd
import seaborn as sns
import numpy as np
import Timer as ti

SAVE_DIR = "C:/this/minor-datascience-shared/classification/"
ti.start_timer("runtime")


def calculateAveragesForStep(feature, feature_at, feature_equal, step_size=0.1):
    """
    Return a dict with the average of the feature per rating step.
    1.0: 43,
    1.1: 23,
    1.2: 11,
    etc
    """
    ranges = []  # the raw classes: 1.1, 2.2 etc
    results = {}  # the sum and counts to calculate average

    for x in np.arange(1.0, 5 + step_size, step_size):  # 1.1, 1.2, 1.3 etc
        label = round(x, 2)
        ranges.append(label)
        # initialize empty results for each color
        results[f"{label}_{feature}_sum"] = 0
        results[f"{label}_{feature}_count"] = 0

    print(f"Working with empty result: {results}")
    print(f"Raw classes: {ranges}")

    # iterate book database, for each book retrieve the feature, and append those values to the corresponding
    # rating in the result dict
    set_table("full_parse_results")
    print("Start parsing.")
    for book_dict in get_all_cursor():
        # skip if feature does not exist
        if feature not in book_dict:
            continue
        if feature_at not in book_dict:
            continue

        # skip if its not a record we want to account
        if book_dict[feature_at] != feature_equal:
            continue

        # retrieve the rating of this book and convert it to the value in the result dict
        rating = str(round(float(book_dict["average_rating"]), 1))

        # skip if label does not exist in result dict: for example 0.0
        if float(rating) not in ranges:
            continue

        # save the sum and amount of times added to sum, to later calculate average
        results[f"{rating}_{feature}_sum"] += float(book_dict[feature])
        results[f"{rating}_{feature}_count"] += 1

    # for each rating, calculate the average and put in result dict
    result = {}
    for x in np.arange(1.0, 5 + step_size, step_size):
        label = round(x, 2)
        sum = results[f"{label}_{feature}_sum"]
        count = results[f"{label}_{feature}_count"]
        # print(f"{label} sum={sum}, count={count}")
        result[label] = 0 if count == 0 else round(sum / count, 2)
    return result


averages = calculateAveragesForStep("amount_of_faces", "genre_0", "science", 0.1)
print(averages)
