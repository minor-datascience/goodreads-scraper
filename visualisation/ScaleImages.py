"""
Script to scale images in a folder to a desired dimension, and save the scaled results to another folder.
Used for machine learning, which expects input to be consistent.
Script sometimes crashes, just run again.
todo: fix black border scale not centered, and that script crashes for no reason
"""

import os
from multiprocessing.pool import ThreadPool

from Timer import *
from PIL import Image

# 1 = keep aspect ratio, introduce black border, 2 = scale and stretch image
SCALE_METHOD = 1

# desired dimensions of each scaled image
DESIRED_WIDTH = 90
DESIRED_HEIGHT = 125

# read and write from these folders
SOURCE_FOLDER = "C:/THIS/minor-data-science-shared/bookcovers/"
RESULT_FOLDER = f"{SOURCE_FOLDER}_{'scaled' if SCALE_METHOD == 1 else 'stretched'}_{DESIRED_WIDTH}x{DESIRED_HEIGHT}/"

# stop program if source folder do not exist
if not os.path.isdir(SOURCE_FOLDER):
    print(f"Source folder {SOURCE_FOLDER} does not exist.")
    quit()

# create result folder if it does not exists
if not os.path.isdir(RESULT_FOLDER):
    print(f"Result folder {RESULT_FOLDER} does not exist, it has been created.")
    os.mkdir(RESULT_FOLDER)

AMOUNT_OF_THREADS = 6


def resize_image(dir_entry):
    try:
        start_timer(dir_entry.name)

        source_img = Image.open(dir_entry.path)

        if SCALE_METHOD == 1:
            # calculate the new dimensions that fit within the desired dimensions, keeping aspect ratio
            source_width, source_height = source_img.size
            new = min(DESIRED_WIDTH / source_width, DESIRED_HEIGHT / source_height)
            result_width = int(round(new * source_width))
            result_height = int(round(new * source_height))
            print(f"Scaling: x={source_width} and y={source_height} to X={result_width} and Y={result_height}")

            # scale the image
            source_img = source_img.resize((result_width, result_height))

            # paste the scaled image on a black background with desired dimensions
            result_img = Image.new("RGB", (DESIRED_WIDTH, DESIRED_HEIGHT))
            source_size = source_img.size
            result_img.paste(source_img, (int((DESIRED_HEIGHT - source_size[0]) / 2),
                                          int((DESIRED_WIDTH - source_size[1]) / 2)))
        elif SCALE_METHOD == 2:
            result_img = source_img.resize((DESIRED_WIDTH, DESIRED_HEIGHT))
        else:
            print(f"Scaled method {SCALE_METHOD} not implemented.")
            quit()
            return

        # save scaled image
        result_img.save(RESULT_FOLDER + dir_entry.name)
        # result_img.show()

        # runtime statistics
        global amount_scaled
        amount_scaled += 1
        print(f"Scaled {amount_scaled}/{amount_to_scale}: {dir_entry.name} in "
              f"{get_elapsed_time(dir_entry.name)}({round(get_elapsed_time('runtime') / amount_scaled)})ms")

    except Exception as e:
        print(f"Failed to scale {dir_entry.path}: {e}")


start_timer("runtime")

# determine files to scale
print("Scanning for files to scale.")
dir_entries = []
for dir_entry in os.scandir(SOURCE_FOLDER):
    # skip if already scaled
    if os.path.isfile(RESULT_FOLDER + dir_entry.name):
        continue

    # skip if file is not a proper image
    try:
        img = Image.open(dir_entry.path)  # exception if it couldn't be opened
        img.verify()  # throws exception if something is wrong with image
        # verify is one nasty method which requires the image to be opened again, thats why this is not in the loop
    except:
        continue

    dir_entries.append(dir_entry)

amount_to_scale = len(dir_entries)
amount_scaled = 0

# start threaded scaling
print(f"Started scaling {amount_to_scale} files.")
pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(func=resize_image, iterable=dir_entries)

print(f"Finished scaling {amount_to_scale} files in {get_elapsed_time('runtime')}ms.")
