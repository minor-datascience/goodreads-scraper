"""
For all the database items containing average R, G and B values, find the average for each rating step and
output those to a CSV.
"""
import numpy as np
from DatabaseTools import *
from Timer import *

SAVE_DIR = "C:/this/minor-datascience-shared/classification/"
start_timer("runtime")
colors = ["red", "green", "blue"]
ranges = []  # the raw classes: 1.1, 2.2 etc
results = {}

step_size = 0.1
for x in np.arange(1.0, 5 + step_size, step_size):  # 1.1, 1.2, 1.3 etc
    label = round(x, 2)
    ranges.append(label)
    # initialize empty results for each color
    for color in colors:
        results[f"{label}_{color}_sum"] = 0
        results[f"{label}_{color}_count"] = 0

print(f"Working with empty result: {results}")
print(f"Raw classes: {ranges}")

# iterate book database, for each book retrieve the colors from colors db, and append those values to the corresponding
# rating in the result dict
set_table("full_parse_results")
count = 0
print("Start parsing.")
for book_dict in get_all_cursor():
    # skip if it does not contain color values
    if "avg_red" not in book_dict:
        continue

    isbn = book_dict["isbn"]

    # retrieve the rating of this book and convert it to the value in the result dict
    rating = str(round(float(book_dict["average_rating"]), 1))

    # skip if label does not exist in result dict: for example 0.0
    if float(rating) not in ranges:
        continue

    # for each red, green and blue value in color
    for color in colors:
        # append color value to total
        results[f"{rating}_{color}_sum"] += book_dict["avg_" + color]
        # calculate amount of times added to total, to later calculate average
        results[f"{rating}_{color}_count"] += 1

    # runtime statistics
    count += 1
    if count % 1000 == 0:
        print(f"Iterated {count}")

print(results)

# calculate the average of each red, green and blue value of each rating and write to file
print("Writing to file.")
with open(f"{SAVE_DIR}color_results_{step_size}.csv", "w") as file:
    # empty file if exists
    file.truncate(0)

    # for each rating, calculate the average
    for x in np.arange(1.0, 5 + step_size, step_size):
        label = round(x, 2)
        for color in colors:
            sum = results[f"{label}_{color}_sum"]
            count = results[f"{label}_{color}_count"]
            if count == 0:
                average = 0
            else:
                average = sum // count

            to_write = f"{label}_{color}_average, {average}\n"
            file.write(to_write)

print(f"Done in {get_elapsed_time('runtime')}ms.")
