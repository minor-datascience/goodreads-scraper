import tools.DatabaseTools as db

rating_sum = 0
rating_total = 0
for book_dict in db.get_all_cursor():
    rating = book_dict["average_rating"]
    rating_sum += float(rating)
    rating_total += 1

average = rating_sum / rating_total
print(f"Average is {average} from {rating_total} records.")
