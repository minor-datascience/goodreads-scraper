# script to devide the book covers ratings by groups
# for example: book cover with rating 4.1 gets thrown
import datetime
import os
import re
import traceback
from multiprocessing.pool import ThreadPool

import pymongo
import numpy as np
import requests

RESULT_FOLDER = "C:/this/labeledbookcovers/"
AMOUNT_OF_THREADS = 100
DATABASE_NAME = "goodreads"
COLLECTION_NAME = "full_parse_results"
runtime_start = datetime.datetime.now()

db_client = pymongo.MongoClient("mongodb://localhost:27017/")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]
amount_downloaded = 0


def remove_image_scaling(image_string):
    if "._SY" not in image_string and "._SX" not in image_string:
        return image_string

    pattern = re.compile(".*(\..*)\.jpg")
    match = pattern.match(image_string)
    if not match:
        return image_string
    to_remove = match.group(1)  # string captured in group 1: something like ._SY475_
    return re.sub(to_remove, "", image_string)


def sort_book(book_dict):
    try:
        start_time = datetime.datetime.now()

        # gather this books info
        isbn = book_dict["isbn"]
        url = book_dict["cover_link"]
        url = remove_image_scaling(url)
        rating = book_dict["average_rating"]

        # determine the closest label
        closest_label = 1.0
        min_diff = 5.0
        for label in labels:
            diff = abs(round(float(rating) - float(label), 3))
            if diff < min_diff:
                min_diff = diff
                closest_label = label

        # skip if already downloaded
        filename = f"{RESULT_FOLDER}{closest_label}/{isbn}.jpg"
        if os.path.isfile(filename):
            return

        # download the book cover
        r = requests.get(url, allow_redirects=True)
        open(filename, 'wb').write(r.content)

        # determine download time for this file
        end_time = datetime.datetime.now()
        time_diff = (end_time - start_time)
        execution_time = int(time_diff.total_seconds() * 1000)

        # determine average download time
        global amount_downloaded
        amount_downloaded += 1
        end_time = datetime.datetime.now()
        time_diff = (end_time - runtime_start)
        full_execution_time = int(time_diff.total_seconds() * 1000)
        avg_parse_time = round(full_execution_time / amount_downloaded)

        # log parse results
        print(f"Downloaded and sorted: isbn: {isbn}, time: {execution_time}({avg_parse_time})ms,"
              f" rating: {rating}, in folder: {closest_label}, link: {url}")
    except:
        print(f"Failed to parse {book_dict}")
        # print(traceback.format_exc())


labels = []
# define label
for x in np.arange(1.0, 6.0, 1.0):
    labels.append(round(x, 2))
print(f"Working with labels: {labels}")

# stop if result folder does not exists
if not os.path.exists(RESULT_FOLDER):
    print(f"Save folder '{RESULT_FOLDER}' does not exist.")
    exit()

# create result folders
for x in labels:
    folder_path = RESULT_FOLDER + str(x)
    if not os.path.exists(folder_path):
        print(f"Creating {folder_path}")
        os.mkdir(folder_path)

# select all isbns
isbn_cursor = db_collection.find({}, {
    "isbn": 1, "cover_link": 1, "average_rating": 1
})

book_dict_list = []
for book_dict in isbn_cursor:
    book_dict_list.append(book_dict)

pool = ThreadPool(AMOUNT_OF_THREADS)
results = pool.map(sort_book, book_dict_list)
