# Scrapers

## Database

MongoDB tutorial to the point: [link](https://www.w3schools.com/python/python_mongodb_getstarted.asp)  
Convert SQL to Mongo: [link](http://www.querymongo.com/)  
Local connection URL: [mongodb://localhost:27017/](mongodb://localhost:27017/)  
Server connection URL: [mongodb://jort:finalround@jort.dev:27017](mongodb://jort:finalround@jort.dev:27017)

## Fix import error

The scripts reference other Python files in the project.  
Set the PYTHONPATH environment variable to the root of the project to avoid import errors.  
Linux:

```
export PYTHONPATH="$PYTHONPATH:/home/jort/this/goodreads-scraper/" 
```

PyCharm does this automatically.

## Google Vision documentation

* [Google Vision demonstration](https://cloud.google.com/vision)
* [Python documentation](https://googleapis.dev/python/vision/latest/index.html)
* [Face detection](https://cloud.google.com/vision/docs/detecting-faces#vision_face_detection-python)
* [Annotation response object](https://cloud.google.com/vision/docs/reference/rest/v1/AnnotateImageResponse)
* [Install on Anaconda](https://anaconda.org/conda-forge/google-cloud-vision)

## Goodreads scraper documentation

* [Example Goodreads page parsed](https://www.goodreads.com/book/show/23692271)
* [Beautifulsoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#quick-start)
* [lxml](https://lxml.de/api/lxml.etree._Element-class.html)

## Setup database locally

1. Install [MongoDB](https://www.mongodb.com/try/download/community)
2. Run KaggleCSVToDatabase.py in tools folder
3. Run Webscraper.py
4. Each parsed book is printed, and values failed to parsed are written as -1
4. See the parsed results in MongoDBCompass with: ```mongodb://localhost:27017/```

## Scripts

A short description of each script in the tools and visualisation folders, and for what it is used.

### DatabaseTools

Functions to interact with the database, to prevent repetitive code in other scripts.

### DeleteFailedPageDownloads

Sometimes, Goodreads returns an empty page. This script removes those downloaded pages by looking at the file size,
which is notably smaller.

### DownloadBookCovers

Multi-threaded script which iterates through all the parsed books in the database, and downloads the book cover image,
and saves that as \emph{isbn.jpg.

### DownloadFullGoogleVisionResponses

Multi-threaded script that passes the first high resolution book cover link as a request to Google Vision, and saves the
JSON-like full response as \emph{isbn.json.

### ExportBookCoverImageURLs

Prints all the book covers URLs comma separated. This was needed whilst exploring the API's, where all the images to
analyze could be passed in one go.

### GoogleVisionParser

Reads the saved JSON-like Google Vision response file, extracts the features needed and appends those to the database.

### KaggleCSVToDatabase

Imports this\cite{anicin_2020 dataset from Kaggle in the Database. ISBNs and book cover URLs found in this database are
used in other scripts.

### ReverseImageSearcher

Reverse image searches the book covers URLs from the Kaggle dataset, and appends the URLs of the first 10 highest
resolution alternatives found to the database.

### Timer

Timer used in most scripts to analyse the performance, to adjust amount of cores used for multi-threading and the
estimated wait time.

### Transferfields

Transfers field from one collection to the other. Used when scripts appended to the wrong collection and to merge data
in one main collection.

### Webscraper

Multi-threaded script that extracts all the features found on a downloaded Goodreads book page and appends those to the
database.

### Util

Some string modification functions used in Webscraper.

### WebpageDownloader

Multi-threaded script which downloads the Goodreads book page of all the ISBNs found in the database as \emph{isbn.html.

### CalculateAverageDimensions

Calculates the average dimensions of all the images in a folder. Used to scale all those images to the same dimensions,
which is needed for the used machine learning algorithms.

### AverageColorCalculator

Calculates the average RGB values of each book cover in a folder, and appends those values to the corresponding ISBN in
the database.

### ColorCalculation

Splits the possible average rating of 1 to 5 into steps, for example 1.0, 1.1, 1.2 etc. For all these rating steps, the
script calculates the average RGB values and saves it to a file. Used for example to see if the red values are higher
for higher rated books.

### PlotColorUsage

Plots the color usage over all ratings and saves the result as image.

### DeleteCorruptImages

Deletes corrupt image files in a folder. Scaling the images did sometimes result in corrupt results.

### DivideData

Multi-threaded script that was used to download book covers and sort them in folders for each rating step, for example
all the books with a rounded rating of 1 get put in one folder, 2 in another folder, etc. Used for a classification
algorithms tried out.

### ScaleImages

Scales images in a folder to a the same dimensions. Either stretches the image, or maintains aspect ratio by introducing
black borders. Needed for the used machine learning algorithms, which expects the images to have the same dimensions.
