"""
import the kaggle database csv into mongodb
(needed for the webscraper to iterate through the ISBNs)
"""
import pymongo
import pandas as pd

DATABASE_NAME = "goodreads"
COLLECTION_NAME = "testing"

db_client = pymongo.MongoClient("mongodb://jort:finalround@jort.dev:27017")

db = db_client[DATABASE_NAME]
db_collection = db[COLLECTION_NAME]

print("Importing CSV in database.")
# create data frame from csv
df = pd.read_csv("../data_cleaned.csv")
# convert df to dict list as 'records', which can be inserted in mongodb
db_collection.insert_many(df.to_dict("records"))
print("Done :3")
